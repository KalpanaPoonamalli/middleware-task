const fs = require("fs")

const log = (req, res, next) => {
    console.log("This is log");
    console.log(`${req.protocol}://${req.get('host')}${req.originalUrl}: ${new Date()} ${req.method} ${req.url}`);

    fs.appendFile("./route/allRequestsFiles.log", `${req.method} ${req.url}` + "\n", (err) => {
        if(err){
            console.log(err)
        } 
    })
    next();
}

module.exports = log;