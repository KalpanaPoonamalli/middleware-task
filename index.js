// write custom middleware that logs all http requests made to a file and use it for all routes in your app
// Then add a new route called /logs that will respond with the contents of the log file .

const express = require("express");
const log = require("./middleware/log.js");
const list = require('./list.js');
const route = require('./route/routeFile')
const fs = require('fs')

const app = express();

app.use(log)

app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));

app.get('/logs', (req, res) => {
    fs.readFile('./route/allRequestsFiles.log','utf-8',(err,data)=>{
        if(err){
            console.log("Error in Reading File",err)
        }else{

            res.setHeader(
                'Content-type',"application/log"
            )
            res.status(200).send(data)
        }

    })
})

app.use('/', route)


const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`)
});

