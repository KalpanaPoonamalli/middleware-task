const list = [
    {
        id: 1,
        name: 'Kalpana',
        status: 'active'
    },
    {
        id: 2,
        name: 'Venky',
        status: 'active'
    },
    {
        id: 3,
        name: 'Ram',
        status: 'inactive'
    }
   
]

module.exports = list;