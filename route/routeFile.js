const express = require('express');
const uuid = require('uuid');
const router = express.Router()
const list = require('../list');


// Get single item 

router.get('/:id', (req, res) => {

    const findingId = list.some(item => {
        return item.id === parseInt(req.params.id);
    });

    if (findingId){
        res.json(list.filter(item => {
            return item.id === parseInt(req.params.id);
        }));
    } else {
        res.status(400).json({
            message: `No item with the id of ${req.params.id}`
        });
    }
});



// create item

router.post('/', (req, res) => {
    //console.log(req.body)
    const newItem = {
        id: uuid.v4(),
        name: req.body.name,
        status: req.body.status
    };

    if (!newItem.name || !newItem.status){
        return res.status(400).json({
            message: "Please include a name and status"
        });
    }
    list.push(newItem);
    res.json(list);
});




// Updata item

router.put('/:id', (req, res) => {
    const findingId = list.some(item => {
        return item.id === parseInt(req.params.id);
    });

    if(findingId){
        const updateItem = req.body;
        list.map(item => {
            if(item.id === parseInt(req.params.id)){
                item.name = updateItem.name ? updateItem.name : item.name;
                item.status = updateItem.status ? updateItem.status : item.status;

                res.json({message: "Item Updated", item})
            }
        })
    }
});




// Delete item

router.delete('/:id', (req, res) => {
    const findingId = list.some(item => {
        return item.id === parseInt(req.params.id);
    });

    if(findingId){
        res.json({
            message: "Item deleted",
            list: list.filter(item => item.id !== parseInt(req.params.id))
        });
    } else {
        res.status(400).json({
            message: `No item with the id of ${req.params.id}`
        });
    }
})




module.exports = router;